# Sharpin session

## Pipeline and cove coverage:
- Master: 
[![pipeline status](https://gitlab.com/thegalang/sharpin-session/badges/master/pipeline.svg)](https://gitlab.com/thegalang/sharpin-session/-/commits/master) [![coverage report](https://gitlab.com/thegalang/sharpin-session/badges/master/coverage.svg)](https://gitlab.com/thegalang/sharpin-session/-/commits/master)
- Development: 
[![pipeline status](https://gitlab.com/thegalang/sharpin-session/badges/session-development/pipeline.svg)](https://gitlab.com/thegalang/sharpin-session/-/commits/session-development) 
[![coverage report](https://gitlab.com/thegalang/sharpin-session/badges/session-development/coverage.svg)](https://gitlab.com/thegalang/sharpin-session/-/commits/session-development)

This is a microservice for the main app (sharpin). This microservice manages the session (Quiz/review) of the user

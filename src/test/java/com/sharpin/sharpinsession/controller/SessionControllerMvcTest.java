package com.sharpin.sharpinsession.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpinsession.service.JsonBuilder;
import com.sharpin.sharpinsession.service.SessionService;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SessionControllerMvcTest {

    @Mock
    private SessionService sessionService;

    @InjectMocks
    private SessionController sessionController = new SessionController();

    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenErrorOccurredItShouldReturnTheCorrectJson() throws IOException {

        String jsonResponse = sessionController.errorOccurred("A123",
            "the error message");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonResponse).get("returned_data");
        assertTrue(returned.get("session_ended").asBoolean());
        assertEquals(1, returned.get("exit_status").asInt());
        assertEquals("the error message", returned.get("error_message").asText());

    }

    @Test
    public void whenNoErrorMessageFoundItShouldGiveDefaultMessage() throws IOException {
        String jsonResponse = sessionController.errorOccurred("A123", null);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonResponse).get("returned_data");
        assertTrue(returned.get("session_ended").asBoolean());
        assertEquals(1, returned.get("exit_status").asInt());
        assertEquals("An error has occurred!", returned.get("error_message").asText());

    }

    @Test
    public void whenErrorOccurredInQuizShouldReturnErrorJson() throws IOException {

        String jsonResponse = sessionController.startQuiz("blablabla");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonResponse).get("returned_data");
        assertTrue(returned.get("session_ended").asBoolean());
        assertEquals(1, returned.get("exit_status").asInt());

    }

    @Test
    public void whenErrorOccurredInReviewShouldReturnErrorJson() throws IOException {

        String jsonResponse = sessionController.startReview("blablabla");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonResponse).get("returned_data");
        assertTrue(returned.get("session_ended").asBoolean());
        assertEquals(1, returned.get("exit_status").asInt());
    }


    @Test
    public void whenErrorOccurredOnAnsweringShouldReturnCorrectJson() throws IOException {

        when(sessionService.answerQuestion("A123", "Uzumaki Naruto"))
            .thenThrow(new NullPointerException("Haha, error!!"));

        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("answer", "Uzumaki Naruto");
        jsonBuilder.addEntry("userId", "A123");
        String jsonString = jsonBuilder.createJson("answer_data");
        String jsonResponse = sessionController.answerQuestion(jsonString);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonResponse).get("returned_data");
        assertEquals("Haha, error!!", returned.get("error_message").asText());
        assertTrue(returned.get("session_ended").asBoolean());
        assertEquals(1, returned.get("exit_status").asInt());
    }

    @Test
    public void whenErrorOccurredShouldRemoveUserData() {

        sessionController.errorOccurred("A123", "the message");
        verify(sessionService).deleteUserData("A123");
    }

    @Test
    public void whenIllegalStateErrorOccuredShouldReturnCorrectJson() throws IOException {

        when(sessionService.answerQuestion("A123", "Uzumaki Naruto"))
            .thenThrow(new IllegalStateException("User has no active session!"));

        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("answer", "Uzumaki Naruto");
        jsonBuilder.addEntry("userId", "A123");
        String jsonString = jsonBuilder.createJson("answer_data");
        String jsonResponse = sessionController.answerQuestion(jsonString);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonResponse).get("returned_data");
        assertEquals("User has no active session!", returned.get("error_message").asText());
        assertTrue(returned.get("session_ended").asBoolean());
        assertEquals(1, returned.get("exit_status").asInt());
    }
}

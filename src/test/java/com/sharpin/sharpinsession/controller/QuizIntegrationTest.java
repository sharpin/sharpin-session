package com.sharpin.sharpinsession.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.card.NormalCard;
import com.sharpin.sharpinsession.card.UserCardList;
import com.sharpin.sharpinsession.repository.UserStateRepository;
import com.sharpin.sharpinsession.service.InteractionService;
import com.sharpin.sharpinsession.service.JsonBuilder;
import com.sharpin.sharpinsession.service.SessionService;
import java.io.IOException;
import java.util.List;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class QuizIntegrationTest {

    @Autowired
    SessionController sessionController;

    @Autowired
    SessionService sessionService;

    @Autowired
    InteractionService interactionService;

    @Autowired
    UserStateRepository userStateRepository;

    private String serializedCards;

    private String testUserId = "A123Galang";

    /**
     * Setting up for tests.
     */
    @BeforeEach
    public void setUp()  {

        interactionService.deleteUserData(testUserId);

        Card card1 = new NormalCard("Card 1",
            "back of card 1", "notes of card 1");
        Card card2 = new NormalCard("Card 2",
            "back of card 2", "notes of card 2");
        Card card3 = new NormalCard("Card 3",
            "back of card 3", "notes of card 3");
        card1.setCardId(100);
        card2.setCardId(200);
        card3.setCardId(300);

        UserCardList cardList = new UserCardList(testUserId);
        cardList.add(card1);
        cardList.add(card2);
        cardList.add(card3);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            serializedCards = objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(cardList);
        } catch (JsonProcessingException e) {
            String dod = "nothing";
        }
    }

    private List<String> getResponsesListFromJson(String jsonString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        JsonNode responsesJson = returned.get("responses");
        String responsesString = mapper.writeValueAsString(responsesJson);
        List<String> responses = mapper.readValue(responsesString, List.class);
        return responses;
    }

    private boolean getSessionEndedFromJson(String jsonString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        return returned.get("session_ended").asBoolean();
    }

    private String setUpAnswerJson(String userId, String answer) {
        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("answer", answer);
        jsonBuilder.addEntry("userId", testUserId);
        return jsonBuilder.createJson("answer_data");

    }

    private int getIntFromJson(String jsonString, String integerField) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        return returned.get(integerField).asInt();
    }

    private String getStringFromJson(String jsonString, String stringField) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        return returned.get(stringField).asText();
    }

    private boolean isJsonFieldExist(String jsonString, String field) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        return returned.has(field);

    }

    @Test
    public void testInteractionsOnQuiz() throws IOException {

        setUp();

        // initiliazing quiz
        String returned = sessionController.startQuiz(serializedCards);

        List<String> responses = getResponsesListFromJson(returned);
        assertEquals(2, responses.size());
        assertEquals("Quiz initialized!", responses.get(0));
        assertEquals("Next question: Card 1", responses.get(1));
        assertFalse(getSessionEndedFromJson(returned));

        // answering first question
        String response = setUpAnswerJson(testUserId, "back of card 1");
        returned = sessionController.answerQuestion(response);

        responses = getResponsesListFromJson(returned);
        assertEquals(3, responses.size());
        assertEquals("Correct answer!", responses.get(0));
        assertEquals(
            "Your notes for this card:\n\nnotes of card 1",
            responses.get(1)
        );
        assertEquals("Next question: Card 2", responses.get(2));
        assertEquals(0, getIntFromJson(returned, "srsUpdate"));

        // answering second question
        response = setUpAnswerJson(testUserId, "back of card 2");
        returned = sessionController.answerQuestion(response);

        responses = getResponsesListFromJson(returned);
        assertEquals(3, responses.size());
        assertEquals("Correct answer!",
            responses.get(0));
        assertEquals(
            "Your notes for this card:\n\nnotes of card 2",
            responses.get(1)
        );
        assertEquals("Next question: Card 3", responses.get(2));
        assertTrue(isJsonFieldExist(returned, "srsUpdate"));

        // answering third
        response = setUpAnswerJson(testUserId, "back of card 70");
        returned = sessionController.answerQuestion(response);

        responses = getResponsesListFromJson(returned);
        assertEquals(3, responses.size());
        assertEquals("Incorrect answer, the correct answer is back of card 3.", responses.get(0));
        assertEquals(
            "Your notes for this card:\n\nnotes of card 3",
            responses.get(1)
        );
        assertEquals("Did you make a typo? (Y/N)", responses.get(2));
        assertFalse(getSessionEndedFromJson(returned));
        assertFalse(isJsonFieldExist(returned, "srsUpdate"));

        // trying to give invalid response
        response = setUpAnswerJson(testUserId, "G");
        returned = sessionController.answerQuestion(response);
        responses = getResponsesListFromJson(returned);
        assertEquals(1, responses.size());
        assertEquals("Please answer (Y/N)!", responses.get(0));
        assertFalse(isJsonFieldExist(returned, "srsUpdate"));

        // giving correct response
        response = setUpAnswerJson(testUserId, "Y");
        returned = sessionController.answerQuestion(response);
        responses = getResponsesListFromJson(returned);
        assertEquals(2, responses.size());
        assertEquals("Your answer is marked as correct!", responses.get(0));
        assertEquals("All cards answered, your session has ended!", responses.get(1));
        assertEquals(0, getIntFromJson(returned, "srsUpdate"));
        assertTrue(getSessionEndedFromJson(returned));

        assertFalse(userStateRepository.findById(testUserId).isPresent());


    }

    @Test
    public void whenCallAnswerOnNoStateShouldGiveError() throws IOException {

        setUp();

        String response = setUpAnswerJson(testUserId, "back of card 1");
        String returned = sessionController.answerQuestion(response);
        assertTrue(getSessionEndedFromJson(returned));
        assertEquals(1, getIntFromJson(returned, "exit_status"));
        assertEquals("User has no active session!", getStringFromJson(returned, "error_message"));
    }
}

package com.sharpin.sharpinsession.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.card.NormalCard;
import com.sharpin.sharpinsession.card.UserCardList;
import com.sharpin.sharpinsession.service.JsonBuilder;
import com.sharpin.sharpinsession.service.SessionService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SessionControllerServiceCallingTest {


    @Mock
    private SessionService sessionService;

    @InjectMocks
    private SessionController sessionController = new SessionController();

    private UserCardList testUserCardList;

    private String userCardListString;

    /**
     * setting up tests.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        testUserCardList = new UserCardList("A123");
        NormalCard card1 = new NormalCard("Card 1",
            "back of card 1", "notes of card 1");
        NormalCard card2 = new NormalCard("Card 2",
            "back of card 2", "notes of card 2");

        card1.setCardId(1);
        card2.setCardId(2);
        testUserCardList.add(card1);
        testUserCardList.add(card2);

        ObjectMapper mapper = new ObjectMapper();
        try {
            userCardListString = mapper.writeValueAsString(testUserCardList);
        } catch (Exception e) {
            String dod = "nothing";
        }
    }

    @Test
    public void whenAnsweringQuestionShouldCallSessionService() {

        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("answer", "Uzumaki Naruto");
        jsonBuilder.addEntry("userId", "D123");
        String answerString = jsonBuilder.createJson("answer_data");

        sessionController.answerQuestion(answerString);
        verify(sessionService).answerQuestion("D123", "Uzumaki Naruto");
    }

    @Test
    public void whenReceiveReviewShouldCallIt() throws Exception {

        sessionController.startReview(userCardListString);

        Class<List<Card>> listClass = (Class<List<Card>>)(Class)List.class;
        ArgumentCaptor<List<Card>> argumentCaptor = ArgumentCaptor.forClass(listClass);
        verify(sessionService, times(1)).initializeReview(eq("A123"), argumentCaptor.capture());

        assertEquals("Card 1", argumentCaptor.getValue().get(0).getFront());
        assertEquals("Card 2", argumentCaptor.getValue().get(1).getFront());

    }

    @Test
    public void whenReceiveQuizShouldCallIt() throws Exception {

        sessionController.startQuiz(userCardListString);

        Class<List<Card>> listClass = (Class<List<Card>>)(Class)List.class;
        ArgumentCaptor<List<Card>> argumentCaptor = ArgumentCaptor.forClass(listClass);
        verify(sessionService, times(1)).initializeQuiz(eq("A123"), argumentCaptor.capture());

        assertEquals("Card 1", argumentCaptor.getValue().get(0).getFront());
        assertEquals("Card 2", argumentCaptor.getValue().get(1).getFront());
    }

}

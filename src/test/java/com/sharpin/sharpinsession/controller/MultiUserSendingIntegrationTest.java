package com.sharpin.sharpinsession.controller;

import static org.junit.jupiter.api.Assertions.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.card.NormalCard;
import com.sharpin.sharpinsession.card.UserCardList;
import com.sharpin.sharpinsession.repository.CardRepository;
import com.sharpin.sharpinsession.repository.QuestionListRepository;
import com.sharpin.sharpinsession.repository.UserStateRepository;
import com.sharpin.sharpinsession.service.InteractionService;
import com.sharpin.sharpinsession.service.SessionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MultiUserSendingIntegrationTest {

    @Autowired
    SessionController sessionController;

    @Autowired
    SessionService sessionService;

    @Autowired
    InteractionService interactionService;

    @Autowired
    UserStateRepository userStateRepository;

    @Autowired
    QuestionListRepository questionListRepository;

    @Autowired
    CardRepository cardRepository;

    private String serializedCards1;
    private String serializedCards2;

    private String serializeListWithUserId(UserCardList userCardList, String userId) {

        userCardList.setUserId(userId);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(userCardList);
        } catch (JsonProcessingException e) {
            return "error when serializing";
        }
    }

    /**
     * Creating the same json for two users and seeing if the program can handle it.
     */
    @Before
    public void setUp() {

        sessionService.deleteUserData("Galang123");
        sessionService.deleteUserData(("Galang321"));

        Card card1 = new NormalCard("Card 1",
            "back of card 1", "notes of card 1");
        Card card2 = new NormalCard("Card 2",
            "back of card 2", "notes of card 2");
        Card card3 = new NormalCard("Card 3",
            "back of card 3", "notes of card 3");
        card1.setCardId(100);
        card2.setCardId(200);
        card3.setCardId(300);

        UserCardList cardList = new UserCardList();
        cardList.add(card1);
        cardList.add(card2);
        cardList.add(card3);

        serializedCards1 = serializeListWithUserId(cardList, "Galang123");
        serializedCards2 = serializeListWithUserId(cardList, "Galang321");

    }

    @Test
    public void testSendingTwoUsersWithSameIdWillWorkOnSameService() {

        sessionController.startQuiz(serializedCards1);
        sessionController.startQuiz(serializedCards2);

        assertEquals(2, questionListRepository.findAll().size());
        assertEquals(6, cardRepository.findAll().size());
        assertTrue(userStateRepository.findById("Galang123").isPresent());
        assertTrue(userStateRepository.findById("Galang321").isPresent());
    }

    @Test
    public void testSendingTwoUsersWithSameIdOnDifferentService() {
        sessionController.startReview(serializedCards1);
        sessionController.startQuiz(serializedCards2);

        assertEquals(2, questionListRepository.findAll().size());
        assertEquals(6, cardRepository.findAll().size());
        assertTrue(userStateRepository.findById("Galang123").isPresent());
        assertTrue(userStateRepository.findById("Galang321").isPresent());
    }

}

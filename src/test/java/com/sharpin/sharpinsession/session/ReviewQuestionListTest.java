package com.sharpin.sharpinsession.session;

import static org.junit.jupiter.api.Assertions.*;

import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.card.NormalCard;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ReviewQuestionListTest {


    private ReviewQuestionList testSubject;

    /**
     setup method for each test suite.
     */
    @BeforeEach
    public void setUp() {

        List<Card> mockedCards = new ArrayList<>();
        for (int temp = 0; temp < 5; temp++) {

            Card tmp = new NormalCard("Food card number " + temp,
                    "number " + temp, "this is mocked card");

            mockedCards.add(tmp);
            tmp.setCardId(temp);
        }

        testSubject = new ReviewQuestionList(mockedCards);
    }


    @Test
    public void testGetNext() {
        assertTrue(testSubject.hasNext());
        String question = testSubject.getNext();
        assertEquals("Food card number 0", question);
    }

    @Test
    public void testGetNextWhenNoElementsLeftShouldReturnSomething() {

        assertTrue(testSubject.hasNext());
        String question = testSubject.getNext();
        assertTrue(testSubject.hasNext());
        question = testSubject.getNext();
        assertTrue(testSubject.hasNext());
        question = testSubject.getNext();
        assertTrue(testSubject.hasNext());
        question = testSubject.getNext();
        assertTrue(testSubject.hasNext());
        question = testSubject.getNext();

        assertFalse(testSubject.hasNext());
        question = testSubject.getNext();
        assertEquals("end of the list", question);

    }

    @Test
    public void testSizeOfList() {
        for (int temp = 0; temp < 5; temp++) {
            assertTrue(testSubject.hasNext());
            String dummy = testSubject.getNext();
            assertEquals("Food card number " + temp, dummy);
        }
        assertFalse(testSubject.hasNext());
    }

    @Test
    public void answerQuestionCorrectlyShouldReturnTrue() {

        String question = testSubject.getNext();
        assertTrue(testSubject.answerQuestion("number 0"));
    }

    @Test
    public void answerQuestionIncorrectlyShouldReturnFalse() {
        String question = testSubject.getNext();
        assertFalse(testSubject.answerQuestion("number 1"));
    }

    @Test
    public void handleResponseTrueShouldIncreaseSrs() {

        Card spyCard = Mockito.spy(new NormalCard("The question", "The answer", "Thenode"));
        List<Card> spyList = new ArrayList<>();
        spyList.add(spyCard);
        testSubject = new ReviewQuestionList(spyList);

        String question = testSubject.getNext();
        testSubject.handleResponse(true);
        assertEquals(1, testSubject.handleResponse(true));
    }

    @Test
    public void handleResponseFalseShouldDecreaseSrs() {

        Card spyCard = Mockito.spy(new NormalCard("The question", "The answer", "Thenode"));
        List<Card> spyList = new ArrayList<>();
        spyList.add(spyCard);
        testSubject = new ReviewQuestionList(spyList);

        String question = testSubject.getNext();
        testSubject.handleResponse(false);
        assertEquals(-1, testSubject.handleResponse(false));
    }

    @Test
    public void getCardNotesShouldReturnCorrectCardNotes() {

        String question = testSubject.getNext();
        assertEquals("this is mocked card", testSubject.getCurrentCardNotes());
    }

    @Test
    public void settingUserIdShouldWork() {

        testSubject.setUserId("A1234");
        assertEquals("A1234", testSubject.getUserId());
    }

    @Test
    public void testEmptyConstructorShouldCreate() {
        testSubject = new ReviewQuestionList();
    }

    @Test
    public void gettingUsrBackShouldDoIt() {

        String question = testSubject.getNext();
        assertEquals("number 0", testSubject.getCurrentCardBack());

    }

    @Test
    public void getingCardIdShouldDoIt() {

        String question = testSubject.getNext();
        assertEquals(0, testSubject.getCurrentCardId());
    }
}

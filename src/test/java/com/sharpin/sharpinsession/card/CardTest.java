package com.sharpin.sharpinsession.card;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CardTest {

    private Card card;

    @BeforeEach
    public void setUp() {
        card = Mockito.mock(Card.class);
    }

    @Test
    public void testMethodGetBack() {
        Mockito.doCallRealMethod().when(card).getBack();
        String back = card.getBack();
        Assertions.assertEquals(null, back);
    }

    @Test
    public void testMethodGetFront() {
        Mockito.doCallRealMethod().when(card).getFront();
        String front = card.getFront();
        Assertions.assertEquals(null, front);
    }

    @Test
    public void testMethodGetNotes() {
        Mockito.doCallRealMethod().when(card).getNotes();
        String notes = card.getNotes();
        Assertions.assertEquals(null, notes);
    }


}

package com.sharpin.sharpinsession.card;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserCardListTest {

    private UserCardList userCardList;

    @BeforeEach
    public void setUp() {
        userCardList = new UserCardList();
    }

    @Test
    public void whenSetUserIdShouldSetIt() {
        userCardList.setUserId("A123");
        Assertions.assertEquals("A123", userCardList.getUserId());
    }
}

package com.sharpin.sharpinsession.card;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NormalCardTest {
    private NormalCard card;

    /**
     * Create a setup.
     */
    @BeforeEach
    public void setUp() {
        card = new NormalCard("pertanyaan", "jawaban", "Notes");
    }

    @Test
    public void testValidateAnswer() {
        Assertions.assertTrue(card.validateAnswer("jawaban"));
    }

    @Test
    public void testUpdateFront() {
        card.setFront("New Notes");
        Assertions.assertEquals(card.getFront(),"New Notes");
    }

    @Test
    public void testUpdateBack() {
        card.setBack("New Answer");
        Assertions.assertEquals(card.getBack(),"New Answer");
    }

    @Test
    public void testUpdateNotes() {
        card.setNotes("New Notes");
        Assertions.assertTrue(card.getNotes().equals("New Notes"));
    }


}

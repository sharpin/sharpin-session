package com.sharpin.sharpinsession.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.repository.CardRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;


public class SessionServiceInteractionTest {

    @Mock
    private InteractionService interactionService;

    @Mock
    private CardRepository cardRepository;

    @InjectMocks
    private SessionService sessionService = new SessionServiceImpl();

    private List<Card> mockList;

    /**
     * setting up tests.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        mockList = new ArrayList<>();
        Card mockCard1 = mock(Card.class);
        Card mockCard2 = mock(Card.class);
        mockList.add(mockCard1);
        mockList.add(mockCard2);

        when(interactionService.getUserState(anyString())).thenReturn("answer question state");
    }

    /**
     * A function to set up stub return value to prevent indexError on answerQuestion.
     */
    private void setUpDummyReturn(String userId, String questionAnswer, String returned) {

        List<String> dummyReturn = new ArrayList<>();
        dummyReturn.add(returned);
        when(interactionService.answerCurrentQuestion(userId, questionAnswer))
            .thenReturn(dummyReturn);
    }

    private int getIntFromJson(String jsonString, String integerField) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        return returned.get(integerField).asInt();
    }

    private boolean getBoolFromJson(String jsonString, String booleanField) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        return returned.get(booleanField).asBoolean();
    }

    private List<String> getListFromJson(String jsonString, String listField) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        JsonNode responsesJson = returned.get(listField);
        String responsesString = mapper.writeValueAsString(responsesJson);
        List<String> responses = mapper.readValue(responsesString, List.class);
        return responses;
    }

    @Test
    public void whenGetNextQuestionOnQuestionsShouldReturnIt() throws IOException {

        when(interactionService.hasSessionEnded("A123")).thenReturn(false);
        when(interactionService.getNextQuestion("A123")).thenReturn("The question");

        JsonBuilder response = sessionService.initializeQuiz("A123", mockList);
        String jsonString = response.createJson("returned_data");

        List<String> responses = getListFromJson(jsonString, "responses");
        assertEquals(2, responses.size());
        assertEquals("Quiz initialized!", responses.get(0));
        assertEquals("Next question: The question", responses.get(1));
    }

    @Test
    public void whenGetNextQuestionOnNoQuestionsShouldNotCallGetNextQuestion() {

        when(interactionService.hasSessionEnded("A123")).thenReturn(true);
        sessionService.initializeReview("A123", mockList);
        verify(interactionService, times(0)).getNextQuestion("A123");
    }

    @Test
    public void whenSessionEndShouldReturnCorrectStatusCode() throws IOException {

        List<String> questionResponse = new ArrayList<>();
        questionResponse.add("Correct answer!");
        questionResponse.add("Your notes for this card:\n\nthe notes");

        when(interactionService.answerCurrentQuestion("C123", "the answer"))
            .thenReturn(questionResponse);
        when(interactionService.getUserState("C123")).thenAnswer(new SessionEnderFlipper());

        JsonBuilder response = sessionService.answerQuestion("C123", "the answer");
        String jsonResponse = response.createJson("returned_data");
        assertTrue(getBoolFromJson(jsonResponse, "session_ended"));
        assertEquals(0, getIntFromJson(jsonResponse, "exit_status"));
    }

    @Test
    public void whenGetNextQuestionOnNoQuestionShouldReturnIt() throws IOException {

        when(interactionService.hasSessionEnded("A123")).thenReturn(true);
        JsonBuilder response = sessionService.initializeReview("A123", mockList);
        String jsonString = response.createJson("returned_data");
        List<String> responses = getListFromJson(jsonString, "responses");

        assertEquals(2, responses.size());
        assertEquals("Review initialized!", responses.get(0));
        assertEquals("All cards answered, your session has ended!", responses.get(1));

    }

    @Test
    public void whenSessionStillExistShouldReturnCorrectStatusCode() throws IOException {

        List<String> questionResponse = new ArrayList<>();
        questionResponse.add("Correct answer!");
        questionResponse.add("Your notes for this card:\n\nthe notes");

        when(interactionService.answerCurrentQuestion("C123", "the answer"))
            .thenReturn(questionResponse);
        when(interactionService.hasSessionEnded("C123")).thenReturn(false);
        when(interactionService.getNextQuestion("C123")).thenReturn("next question");

        JsonBuilder response = sessionService.answerQuestion("C123", "the answer");
        String jsonResponse = response.createJson("returned_data");
        assertFalse(getBoolFromJson(jsonResponse, "session_ended"));
        assertEquals(0, getIntFromJson(jsonResponse, "exit_status"));
    }

    @Test
    public void whenAnsweringQuestionShouldGetThenNextQuestion() throws IOException {

        List<String> questionResponse = new ArrayList<>();
        questionResponse.add("Correct answer!");
        questionResponse.add("Your notes for this card:\n\nthe notes");

        when(interactionService.answerCurrentQuestion("C123", "the answer"))
            .thenReturn(questionResponse);
        when(interactionService.hasSessionEnded("C123")).thenReturn(false);
        when(interactionService.getNextQuestion("C123")).thenReturn("next question");

        JsonBuilder response = sessionService.answerQuestion("C123", "the answer");
        String jsonString = response.createJson("returned_data");
        List<String> responses = getListFromJson(jsonString, "responses");
        assertEquals(3, responses.size());
        assertEquals("Correct answer!", responses.get(0));
        assertEquals("Your notes for this card:\n\nthe notes", responses.get(1));
        assertEquals("Next question: next question", responses.get(2));

    }


    @Test
    public void whenSessionEndedOnCorrectAnswerShouldRemoveUserData() {

        setUpDummyReturn("E123", "the answer", "Correct answer!");
        when(interactionService.hasSessionEnded("E123")).thenReturn(true);
        sessionService.answerQuestion("E123", "the answer");
        verify(interactionService).deleteUserData("E123");
    }

    @Test
    public void whenSessionNotEndedShouldNotRemoveUserData() {

        setUpDummyReturn("E123", "the answer", "free choice");
        when(interactionService.hasSessionEnded("E123")).thenReturn(false);
        sessionService.answerQuestion("E123", "the answer");
        verify(interactionService, times(0)).deleteUserData("E123");
    }


    @Test
    public void whenSessionAnsweredIncorrectlyShouldAskForTypo() throws IOException {

        List<String> questionResponse = new ArrayList<>();
        questionResponse.add("Incorrect answer! the correct answer is this answer");
        questionResponse.add("Your notes for this card:\n\nthe notes");
        when(interactionService.answerCurrentQuestion("E123", "the wrong answer"))
            .thenReturn(questionResponse);

        JsonBuilder jsonBuilder = sessionService.answerQuestion("E123", "the wrong answer");
        String jsonResponse = jsonBuilder.createJson("returned_data");
        List<String> responses = getListFromJson(jsonResponse, "responses");
        assertEquals(3, responses.size());
        assertEquals("Did you make a typo? (Y/N)", responses.get(2));

    }

    @Test
    public void whenSessionAnsweredIncorrectlyShouldSetUserStateToTypoState() {

        setUpDummyReturn("F123", "the answer",
            "Incorrect answer! the is is this");

        sessionService.answerQuestion("F123", "the answer");
        verify(interactionService).setUserState("F123", "typo state");
    }

    @Test
    public void whenSessionAnsweredCorrectlyShouldSetUserStateToAnsweringState() {

        setUpDummyReturn("F123", "the answer",
            "Correct answer!");

        sessionService.answerQuestion("F123", "the answer");
        verify(interactionService).setUserState("F123", "answer question state");
    }

}

package com.sharpin.sharpinsession.service;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

// a class to toggle session ended
// first call will give answer question state
// second call will give no current state
public class SessionEnderFlipper implements Answer {

    int count = 0;

    @Override
    public Object answer(InvocationOnMock invocation) throws Throwable {
        if (count == 0) {
            count++;
            return "answer question state";
        } else {
            return "no current state";
        }
    }
}

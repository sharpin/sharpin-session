package com.sharpin.sharpinsession.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.repository.CardRepository;
import com.sharpin.sharpinsession.session.QuizQuestionList;
import com.sharpin.sharpinsession.session.ReviewQuestionList;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SessionServiceInteractionCallingTest {

    @Mock
    private InteractionService interactionService;

    @Mock
    private CardRepository cardRepository;

    @Mock
    private Card mockCard;

    private List<Card> mockList;

    @InjectMocks
    private SessionService sessionService = new SessionServiceImpl();

    /**
     * Setting up for tests.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockList = new ArrayList<>();
        mockList.add(mockCard);

    }

    @Test
    public void whenStartQuizShouldInitialize() {

        sessionService.initializeQuiz("A123", mockList);
        verify(interactionService, times(1))
            .initializeQuestionList(eq("A123"), any(QuizQuestionList.class));
    }

    @Test
    public void whenStartReviewShouldInitialize() {

        sessionService.initializeReview("A123", mockList);
        verify(interactionService, times(1))
            .initializeQuestionList(eq("A123"), any(ReviewQuestionList.class));
    }

    @Test
    public void whenStartQuizShouldSetStateToAnswering() {

        sessionService.initializeQuiz("B123", mockList);
        verify(interactionService).setUserState("B123", "answer question state");
    }

    @Test
    public void whenStartReviewShouldSetStateToAnswer() {

        sessionService.initializeReview("B123", mockList);
        verify(interactionService).setUserState("B123", "answer question state");
    }

    @Test
    public void whenNoCurrentStateShouldThrowException() {
        when(interactionService.getUserState("B123"))
            .thenReturn("no current state");

        assertThrows(IllegalStateException.class,
            () -> sessionService.answerQuestion("B123", "random answer"));
    }

    @Test
    public void whenAnsweringQuestionShouldCallInteractionService() {

        when(interactionService.getUserState("B123")).thenReturn("answer question state");

        List<String> dummyReturn = new ArrayList<>();
        dummyReturn.add("dummy string");
        when(interactionService.answerCurrentQuestion("B123", "the answer"))
            .thenReturn(dummyReturn);

        sessionService.answerQuestion("B123", "the answer");
        verify(interactionService).answerCurrentQuestion("B123", "the answer");
    }

    @Test
    public void whenDeleteUserDataShouldDelegateToInteractionService() {

        sessionService.deleteUserData("D123");
        verify(interactionService).deleteUserData("D123");
    }


}

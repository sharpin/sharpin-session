package com.sharpin.sharpinsession.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.repository.CardRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SessionServiceTypoStateTest {

    @Mock
    private InteractionService interactionService;

    @Mock
    private CardRepository cardRepository;

    @InjectMocks
    private SessionService sessionService = new SessionServiceImpl();

    private List<Card> mockList;

    /**
     * setting up for tests.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        mockList = new ArrayList<>();
        Card mockCard1 = mock(Card.class);
        Card mockCard2 = mock(Card.class);
        mockList.add(mockCard1);
        mockList.add(mockCard2);

        when(interactionService.getUserState(anyString())).thenReturn("typo state");
    }

    private List<String> getListFromJson(String jsonString, String listField) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode returned = mapper.readTree(jsonString).get("returned_data");
        JsonNode responsesJson = returned.get(listField);
        String responsesString = mapper.writeValueAsString(responsesJson);
        List<String> responses = mapper.readValue(responsesString, List.class);
        return responses;
    }

    @Test
    public void whenIsTypoShouldCallSrsValueWithTrue() {

        sessionService.answerQuestion("E123", "Y");
        verify(interactionService).srsUpdateValue("E123", true);
    }

    @Test
    public void whenIsTypoShouldSetUserStateBackToAnsweringState() {

        sessionService.answerQuestion("E123", "Y");
        verify(interactionService)
            .setUserState("E123", "answer question state");
    }

    @Test
    public void whenIsNotTypoShouldCallSrsValueWithFalse() {

        sessionService.answerQuestion("E123", "N");
        verify(interactionService).srsUpdateValue("E123", false);
    }

    @Test
    public void whenIsNotTypoShouldSetUserStateBackToAnsweringState() {

        sessionService.answerQuestion("E123", "N");
        verify(interactionService)
            .setUserState("E123", "answer question state");

    }

    @Test
    public void whenIsTypoInvalidResponseShouldNotCallSrsUpdateValue() {

        sessionService.answerQuestion("E123", "G");
        verify(interactionService, times(0))
            .srsUpdateValue(eq("E123"), anyBoolean());
    }

    @Test
    public void whenIsTypoInvalidResponseShouldNotChangeUserState() {

        sessionService.answerQuestion("E123", "G");
        verify(interactionService, times(0))
            .setUserState(eq("E123"), anyString());
    }

    @Test
    public void whenIsTypoShouldReturnCorrectResponses() throws IOException {

        when(interactionService.getNextQuestion("E123")).thenReturn("the next question");
        String jsonString = sessionService.answerQuestion("E123", "Y")
            .createJson("returned_data");

        List<String> responses = getListFromJson(jsonString, "responses");
        assertEquals(2, responses.size());
        assertEquals("Your answer is marked as correct!", responses.get(0));
        assertEquals("Next question: the next question", responses.get(1));

    }

    @Test
    public void whenIsNotTypoShouldReturnCorrectResponses() throws IOException {

        when(interactionService.getNextQuestion("E123")).thenReturn("the next question");
        String jsonString = sessionService.answerQuestion("E123", "N")
            .createJson("returned_data");

        List<String> responses = getListFromJson(jsonString, "responses");
        assertEquals(2, responses.size());
        assertEquals("Your answer is marked as incorrect!", responses.get(0));
        assertEquals("Next question: the next question", responses.get(1));
    }

    @Test
    public void whenIsInvalidResponseShouldNotCallGetNextQuestion() {

        sessionService.answerQuestion("E123", "G");
        verify(interactionService, times(0)).getNextQuestion("E123");
    }

    @Test
    public void whenIsInvalidResponseShouldReturnCorrectResponses() throws IOException {

        String jsonString = sessionService.answerQuestion("E123", "G")
            .createJson("returned_data");

        List<String> responses = getListFromJson(jsonString, "responses");
        assertEquals(1, responses.size());
        assertEquals("Please answer (Y/N)!", responses.get(0));
    }

}

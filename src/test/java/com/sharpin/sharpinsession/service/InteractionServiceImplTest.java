package com.sharpin.sharpinsession.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpinsession.repository.QuestionListRepository;
import com.sharpin.sharpinsession.repository.UserState;
import com.sharpin.sharpinsession.repository.UserStateRepository;
import com.sharpin.sharpinsession.session.QuestionList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

public class InteractionServiceImplTest {

    @Mock
    QuestionListRepository questionListRepository;

    @Mock
    UserStateRepository userStateRepository;

    @InjectMocks
    InteractionService interactionService = new InteractionServiceImpl();

    @Mock
    QuestionList randomQuestionList;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenInitializeListShouldCreateUseData() {

        interactionService.initializeQuestionList("A123", randomQuestionList);

        ArgumentCaptor<QuestionList> argumentCaptor = ArgumentCaptor.forClass(QuestionList.class);
        verify(questionListRepository).save(argumentCaptor.capture());
        assertEquals(randomQuestionList, argumentCaptor.getValue());

    }

    @Test
    public void whenInitializeQuestionListShouldDeleteUserDataFirst() {

        interactionService.initializeQuestionList("B123", randomQuestionList);

        verify(questionListRepository).deleteById("B123");
    }

    @Test
    public void whenDeleteShouldCallDeleteRepo() {

        interactionService.deleteUserData("A123");

        verify(questionListRepository).deleteById("A123");
    }

    @Test
    public void whenHasSessionEndedEndShouldReturnIt() {

        when(randomQuestionList.hasNext()).thenReturn(false);

        Optional<QuestionList> returned = Optional.of(randomQuestionList);
        when(questionListRepository.findById("A123")).thenReturn(returned);
        assertTrue(interactionService.hasSessionEnded("A123"));

    }

    @Test
    public void whenSessionNotEndedShouldReturnIt() {

        when(randomQuestionList.hasNext()).thenReturn(true);

        Optional<QuestionList> returned = Optional.of(randomQuestionList);
        when(questionListRepository.findById("A123")).thenReturn(returned);
        assertFalse(interactionService.hasSessionEnded("A123"));
    }

    @Test
    public void whenGetNextQuestionShouldReturnIt() {

        when(randomQuestionList.getNext()).thenReturn("Who is the seventh hokage?");

        Optional<QuestionList> returned = Optional.of(randomQuestionList);
        when(questionListRepository.findById("A123")).thenReturn(returned);
        assertEquals("Who is the seventh hokage?", interactionService.getNextQuestion("A123"));
    }

    @Test
    public void whenAnswerCurrentQuestionRightShouldReturnCorrectFormat() {

        when(randomQuestionList.answerQuestion("Uzumaki Naruto")).thenReturn(true);
        when(randomQuestionList.getCurrentCardNotes())
            .thenReturn("Naruto is the seventh hokage after Kakashi");

        Optional<QuestionList> returned = Optional.of(randomQuestionList);
        when(questionListRepository.findById("A123")).thenReturn(returned);
        List<String> responses = interactionService.answerCurrentQuestion("A123", "Uzumaki Naruto");

        assertEquals("Correct answer!", responses.get(0));
        assertEquals(
            "Your notes for this card:\n"
            + "\n"
            + "Naruto is the seventh hokage after Kakashi", responses.get(1));

    }

    @Test
    public void whenAnswerCurrentQuestionWrongShouldReturnCorrectFormat() {

        when(randomQuestionList.answerQuestion("Uzumaki Naruto")).thenReturn(false);
        when(randomQuestionList.getCurrentCardBack()).thenReturn("Kakashi");
        when(randomQuestionList.getCurrentCardNotes())
            .thenReturn("Naruto is the seventh hokage after Kakashi");

        Optional<QuestionList> returned = Optional.of(randomQuestionList);
        when(questionListRepository.findById("A123")).thenReturn(returned);
        List<String> responses = interactionService.answerCurrentQuestion("A123", "Uzumaki Naruto");

        assertEquals("Incorrect answer, the correct answer is Kakashi.", responses.get(0));
        assertEquals(
            "Your notes for this card:\n"
                + "\n"
                + "Naruto is the seventh hokage after Kakashi", responses.get(1));
    }

    @Test
    public void whenGetNextShouldSaveQuestionList() {

        when(randomQuestionList.getNext()).thenReturn("Who is the seventh hokage?");

        Optional<QuestionList> returned = Optional.of(randomQuestionList);
        when(questionListRepository.findById("A123")).thenReturn(returned);

        interactionService.getNextQuestion("A123");
        ArgumentCaptor<QuestionList> argumentCaptor = ArgumentCaptor.forClass(QuestionList.class);
        verify(questionListRepository).save(argumentCaptor.capture());
        assertEquals(randomQuestionList, argumentCaptor.getValue());
    }

    @Test
    public void whenDeleteShouldDeleteUserStateAndQuestions() {

        interactionService.deleteUserData("F123");
        verify(userStateRepository).deleteById("F123");
        verify(questionListRepository).deleteById("F123");
    }

    @Test
    public void whenDeleteDataDoesNotExistShouldDoNothing() {

        doThrow(new EmptyResultDataAccessException(1))
            .when(questionListRepository).deleteById("A123");
        interactionService.deleteUserData("A123");
    }

    @Test
    public void whenCreateUserStateShouldCallIt() {

        ArgumentCaptor<UserState> argumentCaptor = ArgumentCaptor.forClass(UserState.class);
        interactionService.setUserState("E123", "answer question state");
        verify(userStateRepository).save(argumentCaptor.capture());
        assertEquals("answer question state", argumentCaptor.getValue().getState());
        assertEquals("E123", argumentCaptor.getValue().getUserId());
    }

    @Test
    public void whenGetUserStateOnNoStateGiveBadState() {

        Optional<UserState> returned = Optional.empty();
        when(userStateRepository.findById("A123")).thenReturn(returned);
        assertEquals("no current state", interactionService.getUserState("A123"));
    }

    @Test
    public void whenGetUserStateShouldReturnTheState() {

        UserState userState = new UserState("A123", "typo state");
        Optional<UserState> returned = Optional.of(userState);
        when(userStateRepository.findById("A123")).thenReturn(returned);
        assertEquals("typo state", interactionService.getUserState("A123"));
    }

    @Test
    public void whenGetCurrentCardIdShouldReturnIt() {

        QuestionList mockQuestionList = mock(QuestionList.class);
        when(mockQuestionList.getCurrentCardId()).thenReturn((long) 12);
        when(questionListRepository.findById("B123")).thenReturn(Optional.of(mockQuestionList));
        assertEquals(12, interactionService.getCurrentCardId("B123"));
    }

    @Test
    public void whenGetSrsUpdateValueShouldCallHandleResponse() {

        QuestionList mockQuestionList = mock(QuestionList.class);
        when(questionListRepository.findById("C123")).thenReturn(Optional.of(mockQuestionList));
        interactionService.srsUpdateValue("C123", true);
        verify(mockQuestionList).handleResponse(true);
    }

    @Test
    public void whenGetSrsUpdateValueShouldReturnTheCorrectOne() {

        QuestionList mockQuestionList = mock(QuestionList.class);
        when(mockQuestionList.handleResponse(false)).thenReturn(-1);
        when(questionListRepository.findById("C123")).thenReturn(Optional.of(mockQuestionList));
        assertEquals((long) -1, interactionService.srsUpdateValue("C123", false));
    }


}

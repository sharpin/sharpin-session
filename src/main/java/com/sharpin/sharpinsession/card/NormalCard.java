package com.sharpin.sharpinsession.card;

import javax.persistence.Entity;

@Entity
public class NormalCard extends Card {

    /**
     * Inisialisasi kartu.
     * @param front depan kartu (yang ditanyakan)
     * @param back belakang kartu (yang di check)
     * @param notes notes dari kartu
     */
    public NormalCard(String front, String back, String notes) {
        this.front = front;
        this.back = back;
        this.notes = notes;
    }

    public NormalCard() {}

    @Override
    public boolean validateAnswer(String answer) {
        return back.equals(answer);
    }

    @Override
    public void setFront(String front) {
        this.front = front;
    }

    @Override
    public void setBack(String back) {
        this.back = back;
    }

    @Override
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
package com.sharpin.sharpinsession.card;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "session_card")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = NormalCard.class, name = "NormalCard"),
})
public abstract class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    protected Long elementId;

    @Column(name = "card_id")
    protected Long cardId;

    @Column(name = "front")
    protected String front;

    @Column(name = "back")
    protected String back;

    @Column(name = "notes")
    protected String notes;

    public abstract boolean validateAnswer(String answer);

    public abstract void setFront(String front);

    public abstract void setBack(String back);

    public abstract void setNotes(String notes);

    public String getFront() {
        return front;
    }

    public String getBack() {
        return back;
    }

    public String getNotes() {
        return notes;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long id) {
        this.cardId = id;
    }
}
package com.sharpin.sharpinsession.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.card.UserCardList;
import com.sharpin.sharpinsession.service.JsonBuilder;
import com.sharpin.sharpinsession.service.SessionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SessionController {

    @Autowired
    SessionService sessionService;

    /**
     * Function to initialize a review.
     * @param jsonString string containing serialized cards
     * @return jsonString of questions
     */
    @PostMapping("/startreview")
    public String startReview(@RequestBody String jsonString) {

        ObjectMapper mapper = new ObjectMapper();

        String userId = null;
        List<Card> cards = null;
        try {
            
            UserCardList userCardList = mapper.readValue(jsonString,
                UserCardList.class);

            userId = userCardList.getUserId();
            cards = userCardList.getCardList();
            JsonBuilder jsonBuilder = sessionService.initializeReview(userId, cards);
            return jsonBuilder.createJson("returned_data");

        } catch (Exception e) {
            return errorOccurred(userId, e.getMessage());
        }
    }

    /**
     * Function to initialize a quiz.
     * @param jsonString string containing serialized cards
     * @return jsonString of questions
     */
    @PostMapping("/startquiz")
    public String startQuiz(@RequestBody String jsonString) {

        ObjectMapper mapper = new ObjectMapper();
        String userId = null;
        List<Card> cards = null;
        try {
            UserCardList userCardList = mapper.readValue(jsonString,
                UserCardList.class);

            userId = userCardList.getUserId();
            cards = userCardList.getCardList();
            JsonBuilder jsonBuilder = sessionService.initializeQuiz(userId, cards);
            return jsonBuilder.createJson("returned_data");

        } catch (Exception e) {
            return errorOccurred(userId, e.getMessage());
        }
    }

    /**
     * Function to answer the current question.
     * @param jsonString string containing userId and question answer
     * @return jsonString of responses
     */
    @PostMapping("/answer")
    public String answerQuestion(@RequestBody String jsonString) {

        String userId = null;
        String answer = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode returned = objectMapper.readTree(jsonString).get("answer_data");
            userId = returned.get("userId").asText();
            answer = returned.get("answer").asText();
            JsonBuilder jsonBuilder =  sessionService.answerQuestion(userId, answer);
            return jsonBuilder.createJson("returned_data");
        } catch (Exception e) {
            return errorOccurred("userId", e.getMessage());
        }
    }

    /**
     * If an error occured, this function will return the error.
     * @param userId userId of the user
     * @param message error message
     * @return JsonString of the error
     */
    public String errorOccurred(String userId, String message)  {

        if (message == null) {
            message = "An error has occurred!";
        }
        ObjectMapper objectMapper = new ObjectMapper();

        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("session_ended", true);
        jsonBuilder.addEntry("exit_status", 1);
        jsonBuilder.addEntry("error_message", message);
        sessionService.deleteUserData(userId);
        return jsonBuilder.createJson("returned_data");

    }

}

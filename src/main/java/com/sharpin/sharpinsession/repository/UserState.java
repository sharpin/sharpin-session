package com.sharpin.sharpinsession.repository;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "user_state")
@Entity
public class UserState {

    @Id
    private String userId;

    private String state;

    public UserState() {}

    public UserState(String userId, String state) {
        this.userId = userId;
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public String getUserId() {
        return userId;
    }

}

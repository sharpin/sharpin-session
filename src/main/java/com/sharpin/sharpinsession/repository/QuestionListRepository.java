package com.sharpin.sharpinsession.repository;

import com.sharpin.sharpinsession.session.QuestionList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionListRepository extends JpaRepository<QuestionList, String> {
}

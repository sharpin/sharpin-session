package com.sharpin.sharpinsession.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserStateRepository extends JpaRepository<UserState, String> {
}

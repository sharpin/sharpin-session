package com.sharpin.sharpinsession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SharpinsessionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SharpinsessionApplication.class, args);
    }

}

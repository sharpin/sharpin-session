package com.sharpin.sharpinsession.service;

import com.sharpin.sharpinsession.session.QuestionList;
import java.util.List;

public interface InteractionService {

    String getNextQuestion(String userId);

    List<String> answerCurrentQuestion(String userId, String answer);

    void initializeQuestionList(String userId, QuestionList cards);

    void deleteUserData(String userId);

    boolean hasSessionEnded(String userId);

    void setUserState(String userId, String userState);

    String getUserState(String userId);

    long getCurrentCardId(String userId);

    int srsUpdateValue(String userId, boolean isCardCorrect);
}

package com.sharpin.sharpinsession.service;

import com.sharpin.sharpinsession.card.Card;
import java.util.List;

public interface SessionService {

    JsonBuilder initializeQuiz(String userId, List<Card> cards);

    JsonBuilder initializeReview(String userId, List<Card> cards);

    JsonBuilder answerQuestion(String userId, String answer);

    void deleteUserData(String userId);
}

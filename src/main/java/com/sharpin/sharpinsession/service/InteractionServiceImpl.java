package com.sharpin.sharpinsession.service;

import com.sharpin.sharpinsession.repository.QuestionListRepository;
import com.sharpin.sharpinsession.repository.UserState;
import com.sharpin.sharpinsession.repository.UserStateRepository;
import com.sharpin.sharpinsession.session.QuestionList;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class InteractionServiceImpl implements InteractionService {

    @Autowired
    QuestionListRepository questionListRepository;

    @Autowired
    UserStateRepository userStateRepository;

    @Override
    public String getNextQuestion(String userId) {

        QuestionList questionList = questionListRepository.findById(userId).get();
        String question = questionList.getNext();
        questionListRepository.save(questionList);
        return question;
    }

    @Override
    public List<String> answerCurrentQuestion(String userId, String answer) {

        QuestionList questionList = questionListRepository.findById(userId).get();
        boolean isCorrect = questionList.answerQuestion(answer);

        List<String> responses = new ArrayList<>();
        if (isCorrect) {
            responses.add("Correct answer!");
            responses.add("Your notes for this card:\n\n" + questionList.getCurrentCardNotes());
        } else {
            responses.add("Incorrect answer, the correct answer is "
                + questionList.getCurrentCardBack() + ".");
            responses.add("Your notes for this card:\n\n" + questionList.getCurrentCardNotes());
        }

        return responses;
    }

    @Override
    public void initializeQuestionList(String userId, QuestionList cards) {

        deleteUserData(userId);
        cards.setUserId(userId);
        questionListRepository.save(cards);

    }

    @Override
    public void deleteUserData(String userId) {

        try {
            questionListRepository.deleteById(userId);

        } catch (EmptyResultDataAccessException e) {
            String x = "do nothing";
        }

        try {
            userStateRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            String x = "do nothing";
        }
    }

    @Override
    public boolean hasSessionEnded(String userId) {

        QuestionList questionList = questionListRepository.findById(userId).get();
        return !questionList.hasNext();
    }

    @Override
    public void setUserState(String userId, String state) {

        UserState userState = new UserState(userId, state);
        userStateRepository.save(userState);

    }

    @Override
    public String getUserState(String userId) {

        Optional<UserState> userState = userStateRepository.findById(userId);
        if (userState.isPresent()) {
            return userState.get().getState();
        } else {
            return "no current state";
        }
    }

    @Override
    public long getCurrentCardId(String userId) {

        Optional<QuestionList> questionList = questionListRepository.findById(userId);
        return questionList.get().getCurrentCardId();
    }

    @Override
    public int srsUpdateValue(String userId, boolean isCardCorrect) {

        Optional<QuestionList> questionList = questionListRepository.findById(userId);
        return questionList.get().handleResponse(isCardCorrect);
    }
}

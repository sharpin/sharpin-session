package com.sharpin.sharpinsession.service;

import com.sharpin.sharpinsession.card.Card;
import com.sharpin.sharpinsession.repository.CardRepository;
import com.sharpin.sharpinsession.repository.QuestionListRepository;
import com.sharpin.sharpinsession.session.QuestionList;
import com.sharpin.sharpinsession.session.QuizQuestionList;
import com.sharpin.sharpinsession.session.ReviewQuestionList;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    InteractionService interactionService;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    QuestionListRepository questionListRepository;

    private void insertStatusCodes(String userId, JsonBuilder jsonBuilder) {

        jsonBuilder.addEntry("exit_status", 0);
        jsonBuilder.addEntry("session_ended", hasSessionEnded(userId));
    }

    // if the user does not have any state, then the session has ended.
    private boolean hasSessionEnded(String userId) {
        String userState = interactionService.getUserState(userId);
        return userState.equals("no current state");
    }

    @Override
    public JsonBuilder initializeQuiz(String userId, List<Card> cards) {

        QuestionList questionList = new QuizQuestionList(cards);
        interactionService.initializeQuestionList(userId, questionList);

        JsonBuilder jsonBuilder = new JsonBuilder();

        List<String> response = new ArrayList<>();
        response.add("Quiz initialized!");
        response.add(getNextQuestion(userId));
        jsonBuilder.addList("responses", response);
        jsonBuilder.addEntry("exit_status", 0);
        jsonBuilder.addEntry("session_ended", false);
        return jsonBuilder;
    }

    @Override
    public JsonBuilder initializeReview(String userId, List<Card> cards) {

        QuestionList questionList = new ReviewQuestionList(cards);
        interactionService.initializeQuestionList(userId, questionList);

        JsonBuilder jsonBuilder = new JsonBuilder();

        List<String> response = new ArrayList<>();
        response.add("Review initialized!");
        response.add(getNextQuestion(userId));
        jsonBuilder.addList("responses", response);
        jsonBuilder.addEntry("exit_status", 0);
        jsonBuilder.addEntry("session_ended", false);
        return jsonBuilder;

    }

    @Override
    public JsonBuilder answerQuestion(String userId, String answer)  {

        String state = interactionService.getUserState(userId);

        if (state.equals("typo state")) {
            return handleTypoState(userId, answer);
        } else if (state.equals("answer question state")) {
            return answerNextQuestion(userId, answer);
        } else {
            throw new IllegalStateException("User has no active session!");
        }
    }

    private JsonBuilder answerNextQuestion(String userId, String answer) {


        JsonBuilder jsonBuilder = new JsonBuilder();


        List<String> response = interactionService.answerCurrentQuestion(userId, answer);

        // make a JSON entry to tell the caller that it should change the srs if its correct
        // if its not correct, then should ask the caller if its a typo first
        if (response.get(0).equals("Correct answer!")) {
            addSrsUpdateEntry(userId, true, jsonBuilder);
            response.add(getNextQuestion(userId));
        } else {
            interactionService.setUserState(userId, "typo state");
            response.add("Did you make a typo? (Y/N)");
        }
        jsonBuilder.addList("responses", response);
        insertStatusCodes(userId, jsonBuilder);
        return jsonBuilder;

    }

    private JsonBuilder handleTypoState(String userId, String answer) {

        JsonBuilder jsonBuilder = new JsonBuilder();

        List<String> responses = new ArrayList<>();
        if (answer.equals("Y")) {

            addSrsUpdateEntry(userId, true,  jsonBuilder);
            responses.add("Your answer is marked as correct!");
            responses.add(getNextQuestion(userId));


        } else if (answer.equals("N")) {

            addSrsUpdateEntry(userId, false, jsonBuilder);
            responses.add("Your answer is marked as incorrect!");
            responses.add(getNextQuestion(userId));

        } else {
            responses.add("Please answer (Y/N)!");
        }
        insertStatusCodes(userId, jsonBuilder);
        jsonBuilder.addList("responses", responses);

        return jsonBuilder;

    }

    private  void addSrsUpdateEntry(String userId, boolean isCardCorrect, JsonBuilder jsonBuilder) {

        jsonBuilder.addEntry("srsUpdate", interactionService.srsUpdateValue(userId, isCardCorrect));
        jsonBuilder.addEntry("answeredCardId", interactionService.getCurrentCardId(userId));
    }

    private String getNextQuestion(String userId) {

        if (interactionService.hasSessionEnded(userId)) {

            interactionService.deleteUserData(userId);
            return "All cards answered, your session has ended!";

        } else {

            String nextQuestion = "Next question: " + interactionService.getNextQuestion(userId);
            interactionService.setUserState(userId, "answer question state");
            return nextQuestion;
        }
    }

    @Override
    public void deleteUserData(String userId) {

        interactionService.deleteUserData(userId);
    }


}

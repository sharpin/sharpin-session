package com.sharpin.sharpinsession.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.List;

public class JsonBuilder {

    private ObjectNode currentJson;
    private ObjectMapper objectMapper;

    /**
     * initializing JsonBuilder.
     */
    public JsonBuilder() {

        objectMapper = new ObjectMapper();
        currentJson = objectMapper.createObjectNode();
    }

    /**
     * adding an integer to the json.
     */
    public JsonBuilder addEntry(String key, int value) {
        currentJson.put(key, value);
        return this;
    }

    /**
     * adding a string to the json.
     */
    public JsonBuilder addEntry(String key, String value) {
        currentJson.put(key, value);
        return this;
    }

    /**
     * adding a boolean to the json.
     */
    public JsonBuilder addEntry(String key, boolean value) {
        currentJson.put(key, value);
        return this;
    }

    /**
     * adding an long to the json.
     */
    public JsonBuilder addEntry(String key, long value) {
        currentJson.put(key, value);
        return this;
    }

    /**
     * adding an list to the json.
     */
    public JsonBuilder addList(String key, List<String> value) {
        ArrayNode responseNode = objectMapper.valueToTree(value);
        currentJson.putArray(key).addAll(responseNode);
        return this;
    }

    /**
     * creating the json.
     * @param heading the name of the root of json
     * @return a json of the current jsonBuilder
     */
    public String createJson(String heading)  {

        try {
            JsonNode result = objectMapper.createObjectNode().set(heading, currentJson);
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
        } catch (JsonProcessingException e) {
            return "error_message:" + e.getMessage();
        }
    }

}

package com.sharpin.sharpinsession.session;

import com.sharpin.sharpinsession.card.Card;
import java.util.List;
import javax.persistence.*;
import lombok.NoArgsConstructor;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "question_list")
@NoArgsConstructor
public abstract class QuestionList {

    @Id
    String userId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    protected List<Card> questions;

    @Column(name = "current_index")
    protected int currentIndex;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = true)
    protected Card currentCard;

    public abstract boolean hasNext();

    public abstract String getNext();

    public abstract boolean answerQuestion(String answer);

    public abstract int handleResponse(boolean isCorrect);

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getCurrentCardNotes() {

        return currentCard.getNotes();
    }

    public String getCurrentCardBack() {
        return currentCard.getBack();
    }

    public long getCurrentCardId() {
        return currentCard.getCardId();
    }

}

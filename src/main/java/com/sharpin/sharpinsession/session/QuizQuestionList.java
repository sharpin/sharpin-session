package com.sharpin.sharpinsession.session;

import com.sharpin.sharpinsession.card.Card;
import java.util.List;
import javax.persistence.Entity;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class QuizQuestionList extends QuestionList {

    /**
     * constructor quizquestionlist. Sementara karena belum ada DB.
     *
     * @param cards kartu-kartu yang di encapsulate
     */
    public QuizQuestionList(List<Card> cards) {

        questions = cards;
    }

    public QuizQuestionList() {}

    @Override
    public boolean hasNext() {
        return (currentIndex < questions.size());
    }

    @Override
    public String getNext() {
        if (this.hasNext()) {
            currentCard = questions.get(currentIndex);
            currentIndex++;
            return currentCard.getFront();
        } else {
            // TODO: throw exception
            return "end of the list";
        }
    }

    @Override
    public boolean answerQuestion(String answer) {
        return currentCard.validateAnswer(answer);
    }

    @Override
    public int handleResponse(boolean isCorrect) {
        return 0;
    }

}
